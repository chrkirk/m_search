## How to build

1. Install CocoaPods: `gem install cocoapods`
2. Install pods, in the project's root folder run: `pod install`
3. Open the workspace file `MovieSearch.xcworkspace` in XCode