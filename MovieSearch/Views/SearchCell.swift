//
//  SearchCell.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import UIKit
import SDWebImage

class SeachCell: UITableViewCell {
    
    var item: SearchItem! {
        didSet {
            switch item.mediaType {
            case .movie:
                titleLabel.text  = item.title
                dateLabel.text   = item.releaseDate
            case .tv:
                titleLabel.text  = item.name
                dateLabel.text   = item.firstAirDate
            default:
                break
            }
            
            if let rating = item.voteAverage {
                ratingLabel.text = "Rating: \(rating)"
            } else {
                ratingLabel.text = "No rating available"
            }
            
            if let posterUrl = Service.shared.imageUrl(urlString: item.posterPath) {
                posterImageView.sd_setImage(with: posterUrl)
            }
        }
    }
    
    let posterImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 4
        iv.backgroundColor = UIColor(white: 0.95, alpha: 1)
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Movie Title"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        return label
    }()
    
    let ratingLabel: UILabel = {
        let label = UILabel()
        label.text = "Rating: 5.9"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .lightGray
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "23 Jan 2020"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .lightGray
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        posterImageView.translatesAutoresizingMaskIntoConstraints = false
        posterImageView.heightAnchor.constraint(equalToConstant: SearchVC.cellHeight - 16).isActive = true
        posterImageView.widthAnchor.constraint(equalTo: posterImageView.heightAnchor, multiplier: 2/3).isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [
            posterImageView,
            VerticalStackView(arrangedSubViews: [
                titleLabel,
                ratingLabel,
                dateLabel,
                UIView()
                ]),
            UIView()
            ])
        stackView.spacing = 12
        stackView.alignment = .center
        
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
