//
//  BackdropCell.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 05/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import UIKit

class BackdropCell: UITableViewCell {
    
    @objc let backdropImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 4
        return iv
    }()
    
    @objc let spinner: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .whiteLarge)
        aiv.color = .darkGray
        aiv.startAnimating()
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backdropImageView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        addSubview(backdropImageView)
        backdropImageView.translatesAutoresizingMaskIntoConstraints = false
        backdropImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        backdropImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        backdropImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        backdropImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        spinner.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        spinner.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
