//
//  Service.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import Foundation

class Service {
    static let shared = Service()
    
    private static let apiKey = "6b2e856adafcc7be98bdf0d8b076851c"
    
    private static let searchBaseUrl = "https://api.themoviedb.org/3/search/multi"
    
    private static let imageBaseUrl = "https://image.tmdb.org/t/p/"
    
    private static let trailerBaseUrl = "https://www.youtube.com/watch"
    
    private func url(fromBaseUrl baseUrl: String, withParams params: [String: String]? = [:]) -> URL? {
        guard var components = URLComponents(string: baseUrl) else {
            print("Service.url(withParams: \(String(describing: params)): Unable for create url components from base url")
            return nil
        }
        
        var queryItems = [URLQueryItem]()
        let baseParams = ["api_key": Service.apiKey]
        
        baseParams.forEach { (key, value) in
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }
        
        if let params = params {
            params.forEach { (key, value) in
                let item = URLQueryItem(name: key, value: value)
                queryItems.append(item)
            }
        }
        
        components.queryItems = queryItems
        return components.url
    }
    
    func imageUrl(urlString: String?, size: ImageSize = .thumbnail) -> URL? {
        guard let urlString = urlString, !urlString.isEmpty else {
            return nil
        }
        return URL(string: "\(Service.imageBaseUrl)\(size.rawValue)\(urlString)")
    }
    
    func videoTrailerUrl(from result: VideoResult) -> URL? {
        guard let results = result.results else { return nil }
            
        for videoItem in results {
            if let key = videoItem.key, videoItem.site == "YouTube", videoItem.type == "Trailer" {
                return url(fromBaseUrl: Service.trailerBaseUrl, withParams: ["v": key])
            }
        }
        return nil
    }
    
    func fetchVideoResults(forItem item: SearchItem, completion: @escaping (VideoResult?, Error?) -> Void) {
        guard let id = item.id,
            let url = url(fromBaseUrl: "https://api.themoviedb.org/3/\(item.mediaType.rawValue)/\(id)/videos") else {
                return
        }
        fetchGenericJSON(url: url, completion: completion)
    }
    
    func fetchSearchResults(forSearchTerm searchTerm: String, completion: @escaping (SearchResult?, Error?) -> Void) {
        guard let url = url(fromBaseUrl: Service.searchBaseUrl, withParams: ["query": searchTerm]) else { return }
        fetchGenericJSON(url: url, completion: completion)
    }
    
    func fetchDetailResults(forItem item: SearchItem, completion: @escaping (DetailResult?, Error?) -> Void) {
        guard let id = item.id,
            item.mediaType == .movie || item.mediaType == .tv,
            let url = url(fromBaseUrl: "https://api.themoviedb.org/3/\(item.mediaType.rawValue)/\(id)") else {
                return
        }
        fetchGenericJSON(url: url, completion: completion)
    }
    
    func fetchGenericJSON<T: Decodable>(url: URL, completion: @escaping (T?, Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Service.fetchGenericJSON, \(error)")
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200 else {
                    print("Service.fetchGenericJSON, Unexprected response or status code")
                    completion(nil, error)
                    return
            }
            
            if let mimeType = httpResponse.mimeType, mimeType == "application/json",
                let data = data {
                do {
                    let results = try JSONDecoder().decode(T.self, from: data)
                    completion(results, nil)    // successful fetch
                } catch {
                    print("Service.fetchGenericJSON, Decoding failed, \(error)")
                    completion(nil, error)
                }
            }
        }.resume()
    }
}

// Available sizes for images
enum ImageSize: String {
    case thumbnail = "w500"
    case original  = "original"
}
