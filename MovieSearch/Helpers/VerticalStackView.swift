//
//  VerticalStackView.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import UIKit

class VerticalStackView: UIStackView {
    
    init(arrangedSubViews: [UIView]) {
        super.init(frame: .zero)
        self.axis = .vertical
        
        arrangedSubViews.forEach { addArrangedSubview($0) }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
