//
//  ShowDetailsVC.h
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

@import UIKit;

@interface ShowDetailsVC : UITableViewController

@property (nonatomic, strong) NSURL *backdropUrl;

@property (nonatomic, strong) NSURL *trailerUrl;

@property (nonatomic, strong) NSString *itemTitle;

@property (nonatomic, strong) NSString *overview;

@property (nonatomic, strong) NSString *genre;

@end
