//
//  SearchVC.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import UIKit

class SearchVC: UITableViewController, UISearchBarDelegate {
    
    private var results = [SearchItem]()
    
    private let cellId = "cellId"
    
    static let cellHeight: CGFloat = 200
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var timer: Timer?
    
    private let backgroundView: UILabel = {
        let label = UILabel()
        label.text = "Search for movies or tv shows"
        label.textColor = .lightGray
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Search"
        
        tableView.backgroundView = backgroundView
        
        tableView.register(SeachCell.self, forCellReuseIdentifier: cellId)
        tableView.rowHeight = SearchVC.cellHeight
        tableView.separatorStyle = .none
        
        setupSearchBar()
    }
    
    
    // MARK: - Handling the search bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // add some delay between searches
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { _ in
            Service.shared.fetchSearchResults(forSearchTerm: searchText) { (res, err) in
                if let err = err {
                    print("SearchVC.searchBar(\(searchBar) textDidChange: \(searchText)), \(err)")
                    return
                }
                
                var filteredResults = [SearchItem]()
                if let results = res?.results {
                    for item in results {
                        if item.mediaType == .movie || item.mediaType == .tv {
                            filteredResults.append(item)
                        }
                    }
                }
                self.results = filteredResults
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    private func setupSearchBar() {
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    // MARK: -
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        backgroundView.isHidden = results.count != 0
        return results.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SeachCell
        cell.item = results[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SeachCell
        let detailsController = ShowDetailsVC()
        navigationController?.pushViewController(detailsController, animated: true)
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        Service.shared.fetchDetailResults(forItem: cell.item) { (res, err) in
            dispatchGroup.leave()
            detailsController.overview = res?.overview ?? ""
            detailsController.genre = res?.genres?.first?.name ?? ""
            if let backdropPath = res?.backdropPath, let url = Service.shared.imageUrl(urlString: backdropPath, size: .original) {
                detailsController.backdropUrl = url
            }
        }
        
        dispatchGroup.enter()
        Service.shared.fetchVideoResults(forItem: cell.item) { (res, err) in
            dispatchGroup.leave()
            if let trailerUrl = Service.shared.videoTrailerUrl(from: res!) {
                detailsController.trailerUrl = trailerUrl
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            detailsController.navigationItem.title = cell.titleLabel.text
            detailsController.itemTitle = cell.titleLabel.text
            detailsController.tableView.reloadData()
        }
    }
}
