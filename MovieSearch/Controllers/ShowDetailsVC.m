//
//  ShowDetailsVC.m
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

#import "ShowDetailsVC.h"
#import "MovieSearch-Swift.h"
@import SDWebImage;

@implementation ShowDetailsVC

NSString *const cellId = @"cellId";
NSString *const backdropCellId = @"backdropCellId";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;

    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:cellId];
    [self.tableView registerClass:[BackdropCell self] forCellReuseIdentifier:backdropCellId];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {       // backdrop cell
        BackdropCell *cell = [[BackdropCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:backdropCellId];
        [cell.backdropImageView sd_setImageWithURL:self.backdropUrl completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [cell.spinner stopAnimating];
        }];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    } else if (indexPath.row == 1) {    // trailer cell
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.textLabel.numberOfLines = 0;
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text = @"Watch the trailer";
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
        cell.textLabel.textColor = self.view.tintColor;
        if (self.trailerUrl == nil) {
            [cell setHidden:true];
        }
        return cell;
    } else if (indexPath.row == 2) {    // title cell
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.textLabel.text = self.itemTitle;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
        return cell;
    } else if (indexPath.row == 3) {    // genre cell
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.textLabel.text = self.genre;
        cell.textLabel.textColor = UIColor.lightGrayColor;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
        return cell;
    } else {                            // overview cell
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.textLabel.numberOfLines = 0;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.textLabel.text = self.overview;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [[UIApplication sharedApplication] openURL:self.trailerUrl options:@{} completionHandler:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:         // backdrop cell
            return 280;
        case 1:         // trailer cell
        case 3:         // genre cell
            return 30;
        default:
            return UITableViewAutomaticDimension;
    }
}

@end
