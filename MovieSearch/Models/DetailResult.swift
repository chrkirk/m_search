//
//  DetailResult.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 05/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import Foundation

struct DetailResult: Decodable {
    let backdropPath: String?
    let overview: String?
    let genres: [Genre]?
    
    // movie specific
    let title: String?
    
    // tv-show specific
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case overview, genres, title, name
        case backdropPath = "backdrop_path"
    }
}

struct Genre: Decodable {
    let id: Int?
    let name: String?
}
