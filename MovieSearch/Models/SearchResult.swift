//
//  SearchResult.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 04/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import Foundation

struct SearchResult: Decodable {
    let results: [SearchItem]
}

struct SearchItem: Decodable {
    let id: Int?
    let mediaType: MediaType
    let voteAverage: Float?
    let posterPath: String?
    
    // specific to movies
    let title: String?
    let releaseDate: String?
    
    // specific to tv shows
    let name: String?
    let firstAirDate: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, name
        case mediaType    = "media_type"
        case voteAverage  = "vote_average"
        case posterPath   = "poster_path"
        case releaseDate  = "release_date"
        case firstAirDate = "first_air_date"
    }
}

enum MediaType: String, Decodable {
    case movie  = "movie"
    case tv     = "tv"
    case person = "person"
}
