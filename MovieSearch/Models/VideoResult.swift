//
//  VideoResult.swift
//  MovieSearch
//
//  Created by Christos Kirkos on 06/05/2019.
//  Copyright © 2019 chrkirk. All rights reserved.
//

import Foundation

struct VideoResult: Decodable {
    let results: [VideoItem]?
}

struct VideoItem: Decodable {
    let id: String?
    let key: String?
    let name: String?
    let site: String?
    let size: Int?
    let type: String?
}

